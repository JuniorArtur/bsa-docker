FROM nginx:alpine
LABEL maintainer="ar2rstecenk0@gmail.com"
VOLUME /usr/share/nginx/html
EXPOSE 80
